<?php
    foreach($_GET as $key => $val) { $$key = $val; }
    foreach($_POST as $key => $val) { $$key = $val; }

    /**
     * $name
     * $email
     * $subject
     * $message
     */

    $message = $name . "\r\n\r\n" . $message;

    $to      = 'josh@joshmccreight.com';
    $subject = !empty( $subject ) ? $subject : '';
    $subject = 'Contact from JoshMcCrieght.com - ' . $subject;
    $message = !empty( $message ) ? $message : 'no message detected.';
    $headers = 'From: ' . $email . "\r\n" .
               'Reply-To: ' . $email . "\r\n" .
               'X-Mailer: PHP/' . phpversion();

    $result = mail($to, $subject, $message, $headers, '-fjosh@joshmccreight.com');

    $folder = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'contacts';
    if(!is_dir( $folder ) ) {
        mkdir( $folder, 0777 );
        chmod( $folder, 0777 );
    }
    $file = $folder . DIRECTORY_SEPARATOR . date( 'm-d-Y-' ) . microtime(true) . '.txt';
    $fp = fopen( $file, 'w+' );
    fwrite( $fp, serialize( $_POST ) );
    fclose( $fp );

    if( $result )
        echo 'OK';
    else
        echo 'There was a problem sending the contact message. Please try again later.';
    exit;