var jump=function(e){e.preventDefault();var target = $(this).attr("href");$('html,body').animate({scrollTop: $(target).offset().top - 250},700);}
var jump2=function(e){e.preventDefault();var target2 = $(this).attr("href");$('html,body').animate({scrollTop: $(target2).offset().top - 312},700);}

$(document).ready(function(){
    $('ul li a').click(function(){
        $(this).parent().parent().find('li a.active').removeClass('active');
        $(this).addClass('active');
    });
    $('li.sidebarLi a').click(jump2);
    $('li.yearsLi a').click(jump);


var overlayClick = true;
$('.formOverlay').css({opacity:0.9});

$('a.openForm').click(function(e){
    e.preventDefault();
    $('.formWrap').show().find('div.formOverlay').fadeIn(1000,function(){
        $('.formWrap').find('div.formBox').slideDown(600);
    });
});

$('div.formOverlay').click(function(event){
    if(overlayClick){
        if(event.target == this){
            $('a.closeForm').click();
        }
    }
});

$('a.closeForm').click(function(){
    $('div.formBox').delay(300).slideUp(400, function(){
        $(this).siblings().fadeOut(1000, function(){
            var msgSent = $('.notification_ok');
            if(!msgSent){
                $(".note").empty();
            }
            $('div.formBox').hide();
            $('div.formOverlay').hide();
            $('div.formWrap').hide();
        });
    });

});

/////////////////////
// FORM VALIDATION //
/////////////////////

$(".contactForm").submit(function(){

    var str = $(this).serialize();

    $.ajax({
        type: "POST",
        url: "contact.php",
        data: str,
        success: function(msg){
            if(msg == 'OK' || msg == 'ok') {
                var result = $('<div class="notificationOk">Your message has been sent. Thank you!</div>');
                $('#note').html('');
                $('#note').append(result);
                $(".contactForm").fadeTo(500, 0.01).slideUp(500);
            } else {
                $('#note').html(msg);
            }
        }

    });

    return false;

});
});