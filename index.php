﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Josh McCreight</title>
    <meta name="author" content="Josh McCreight - joshmccreight.com - jlcoresystems.com">
    <meta name="description" content="All about Josh McCreight. Resume, experience, and more.">
    <meta name="keywords" content="josh mccreight,josh,mccreight,victoria,victoria bc,victoria british columbia,british columbia,senior developer,web development,php,lamp,html,css2,javascript,jqeuery,extjs,symfony,problem solving,project management">

    <link href="assets/css/reset.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/960_24.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/styles.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/print.css" rel="stylesheet" type="text/css" media="print" />

    <script type="text/javascript" src="assets/js/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="assets/js/resume.js"></script>

    <!-- IE 7 setting -->
    <!--[if lte IE 7]>
    <link rel="stylesheet" type="text/css" href="assets/css/ie.css" media="all" />
    <![endif]-->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-44747110-1', 'joshmccreight.com');
        ga('send', 'pageview');

    </script>
</head>
<body>
<div id="wrap">
    <!-- Start of Wrap Header -->
    <div id="wrapHeader">
        <!-- Header starts here - This is fixed to the top of the window -->
        <div id="header">
            <!-- 960gs div -->
            <div class="container_24">
                <div id="topBar">
                    <!-- Years on the top goes here -->
                    <div class="grid_13" id="years">
                        <ul id="yearsNav">
                            <li class="yearsLi firstYear"><a href="#y2003">2003</a></li>
                            <li class="yearsLi lastYear"><a href="#wrap" class="active">2011</a></li>
                        </ul>
                    </div>
                    <div class="grid_11" id="topBarRight">
                        <h2>Do what you love &amp; <em>Success Will Follow</em></h2>
                    </div>
                </div>
                <div id="information">
                    <div class="grid_13" id="info">
                        <h2>Josh@JoshMcCreight.com<br /><em>(250)589.5855</em></h2>
                    </div>
                    <div class="grid_11" id="name">
                        <h1>
                            <a href="#wrap"><span>Josh</span> <br /><em>McCreight</em></a>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="wrapContent" class="container_24">
        <div id="content" class="grid_13">
            <div style="text-align:center">
                <a href="resume.doc" target="_blank">Resume.doc</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="resume.docx" target="_blank">Resume.docx</a>
            </div>
            <div class="yearPanel" id="y2011">
                <div class="resumeEntry">
                    <div class="when grid_3">
                        <p>2011<br/><em>~ongoing</em></p>
                    </div>
                    <div class="description grid_9">
                        <h3>Neverblue Media <em>Digital Content Developer</em></h3>
                        <h5><a href="http://www.neverblue.com" target="_blank">www.neverblue.com</a></h5>
                        <p>Primary developer on all properties projects including:<br>
                            <span><a href="http://www.netdegree.com" target="_blank">netdegree.com</a> - A school leads generating site<br></span>
                            <span><a href="javascript:void(0);" target="_blank">bargain.com (no longer active)</a> - Daily deals and bargains<br></span>
                            <span><a href="http://www.travelace.com" target="_blank">travelace.com</a> - Travel deals, tips and information<br></span>
                            <span><a href="http://www.foxysingles.com" target="_blank">foxysingles.com</a> - Singles dating<br></span>
                            <span><a href="http://www.autoinsureusa.com" target="_blank">autoinsureusa.com</a> - Insurance leads generating site <br></span>
                            <span><a href="javascript:void(0);" target="_blank">hostandpost.com (internal)</a> - Leads/conversions tracking <br></span>
                            and several others. Directly working with the main network to create appropriately designed and functional landing pages for affiliates and publishers alike.<br>
                            My main focus was on developing custom solutions for both Neverblue and its affiliates using Symfony1.4 and Symfony2.x. A lot of time was spent in converting old projects to the new Symfony2.x framework, along with a unique, one of a kind form generator used on <a href="http://www.netdegree.com/study/schools">Netdegree.com</a> (near real-time processing of forms from the database with up to 700,000 field and element dependencies).
                            <br>
                        </p>
                    </div>
                    <div class="clear">&nbsp;</div>
                </div>
            </div>

            <div class="yearPanel" id="y2003">
                <div class="resumeEntry">
                    <div class="when grid_3">
                        <p>2003<br/><em>~2011</em></p>
                    </div>
                    <div class="description grid_9">
                        <h3>MK2 Business Solutions <em>Lead Developer</em></h3>
                        <h5><a href="http://www.mk2solutions.com">www.mk2solutions.com</a></h5>
                        <p>
                            Reporting to the President and responsible for all aspects of (personal or corporate website development) programming for the company<br><br>
                            <span>&nbsp;&nbsp;Contribute to the recruiting and selection process for new programmers<br></span>
                            <span>&nbsp;&nbsp;Participate in client meetings and ensure understanding of their needs and expectations<br></span>
                            <span>&nbsp;&nbsp;Assess information garnered during client meetings to create or refine product specifications<br></span>
                            <span>&nbsp;&nbsp;Provide input into methods for increasing revenue generation or improving effectiveness<br></span>
                            <span>&nbsp;&nbsp;Work alone or in project teams to ensure timely completion of projects<br></span>
                            <br>
                            <span><a href="http://www.adventuretech.ca/" target="_blank">Adventuretech</a> - AdventureTech is a Victoria based company that manufactures safe LED tail lights for motorcycles. They are also in development of other uses for highly efficient LED technology. We handled all of the graphic, print, website design, consultation and hosting needs for the startup of Adventure Tech.</span>
                            <span><a href="" target="_blank">CanamWalking (no longer active)</a> - Canamwalking is a pedometer tracking service, health & wellness company located in Victoria, BC. Canamwalking is dedicated to enhancing health through walking programs.</span>
                            <span><a href="http://www.speakwell.ca/" target="_blank">Speakwell.com (has since been sold and changed designs)</a> - Speakwell a Pacific Rim Speaking and Wellness Solutions Inc. company that also owns CanamWalking.com is a wellness and speaking provider based out of Victoria, BC. Founded by Dr. Martin Collis, one of the four "Pioneers of Wellness" in Canada, Speakwell is at the forefront of the wellness industry in North America and one of the most innovative speakers' bureaus in the nation. We provide website design, graphic/logo design, IT support and hosting solutions for Speakwell.</span>
                            <span><a href="http://www.ilmb.gov.bc.ca/" target="_blank">BC Provincial Government (ILMB)</a> - The Integrated Land Management Bureau - Conversion from their old records systems to a usable Drupal format.</span>
                            <span><a href="http://www.craigwalters.net/" target="_blank">Craig Walters Group</a> - Real estate listing site for a local Victoria Company.</span>
                            <span><a href="javascript:void(0)" target="_blank">C.P.L. Property Loans (has since been terminated)</a> - Commercial Property loans is an online provider for large commercial and multifamily dwelling property loans. We provided Graphic design and website development services for Commercial Property Loans website.</span>
                            <span><a href="http://www.internationalalpacas.com/" target="_blank">International Alpacas</a> - International Alpacas of Canada is located on a beautiful 40-acre ranch in Cowichan Valley, Vancouver Island, British Columbia, Canada. This is the home of the Sawyer family, one hour north of beautiful Victoria, known as Canada's garden city. We provided logo/print and website design and hosting services for International Alpacas of Canada.</span>
                            <span><a href="javascript:void(0);" target="_blank">Sawyer & Sawyer (no longer active)</a> - Sawyer & Sawyer helps to chart your financial future through planning and Segfunds that protect your future financial security. We provided logo, website and hosting services for Sawyer & Sawyer.</span>
                            <span><a href="http://www.wildernesstrekking.com/" target="_blank">Wilderness Trekking</a> - Wilderness Trekking is an adventure travel company specializing in unparalled trekking experiences in Nepal, Bhutan, Everest Base Camp and other Indian locations . We handled all of the graphic, print, website design, consultation and hosting needs for the startup of Wilderness Trekking.</span>
                        </p>
                    </div>
                    <div class="clear">&nbsp;</div>
                </div>
            </div>
        </div>

        <div id="sidebar" class="grid_11">
            <ul id="sidebarNav">
                <li class="sidebarLi"><a href="#wrap" class="active">Profile</a></li>
                <li class="sidebarLi"><a href="#education">Education</a></li>
                <li class="sidebarLi"><a href="#skills">Skills </a></li>
                <li><a href="javascript:void(0);" class="openForm">Contact</a></li>
            </ul>

            <div id="sidebarContent" class="grid_8">
                <!-- start of profile info (sidebar) -->
                <div id="profile" class="sidebarPanel">
                    <h4>I love what I do and it shows</h4>
                    <p>
                        Web Development consumes me and I live code; therefore, I am fully immersed in my passion AND my profession.  I am fortunate to be very talented in my work which I have a lot of fun doing. I am always looking for new ways to expand my skill-set and learn new things. These attributes ultimately bring value to my employer by means of my thirst for creating solutions. I am skilled at creating reliable and reusable code so that companies which I work for will be able to focus more on expandability rather than maintenance. Ultimately I will contribute to facilitating product line / business growth and profitability.<br>I am a firm believer in <strong>OOP/MVC</strong> structures.
                    </p>
                    <p class="box">
                        <span>Confident and talented professional Web Developer with uncommonly strong energy &amp; aptitude</span>
                        <span>10+ years of experience in PHP programming and a long list of successful web development projects</span>
                        <span>‘Lives & breathes’ programming, taking pride in ability to overcome the ‘impossible challenges’</span>
                        <span>Skilled in object-oriented programming, building contemporary, clean, readable and reusable code</span>
                        <span>Adept in understanding / interpreting extremely complex information into user friendly language</span>
                        <span>Spatially aware & intuitive - understanding ‘the matrix’, building solutions ahead of the trends</span>
                        <span>Hard working &amp; dedicated with intrinsic motivation coming from creating solutions from problems</span>
                        <span>Driven to stay current in knowledge and well ahead of consumer trends</span>
                    </p>
                    <p>
                        When I am not working on a project for a company, I am working on projects of my own. I have a hard time stopping and am always pushing myself to keep up to date and learn new things.<br>
                        <br>
                        <strong>Some of the most recent projects I have are:</strong><br>
                        <span><a href="javascript:void(0);" title="Sorry, no demos yet!">Easy HTML5 Ad Server/Inventory Management System</a> - This will be an ad server and an inventory management system which is capable of delivering ads and product information from our central database to the developers website. Uses a new technique to deliver ads and information quickly and easily without additional parsing of data on the end-users system.</span>
                        <span><a href="http://jldemos.com/parallax" title="Parallax jQuery Plugin">Parallax jQuery Plugin</a> - A jQuery plugin for a different way of scrolling through a site. (STILL UNDER DEVELOPMENT). Designed for single page sites to scroll through the content in an unconventional way.</span>
                    </p>
                </div>
                <div id="education" class="sidebarPanel">
                    <h4>Education</h4>
                    <p>2006 - 2010<br /> Bachelors of Computer Science<br />
                        <em>University of Victoria</em><br>
                        Option - Math intensive, theory and concepts behind modern programming practices and optimizing
                        database structures. Emphasis on Object-Oriented programming.
                    </p>
                    2000 - 2001<br />
                    <p>New Media Studies Diploma<br />
                        <em>Computer Master Victoria BC</em><br>
                        Flash, Macromedia, Computer animation and contemporary web/graphic design.</p>
                </div>

                <div id="skills" class="sidebarPanel">
                    <div class="grid_4 alpha">
                        <h4>Software</h4>
                        <ul>
                            <li>Photoshop</li>
                            <li>Illustrator</li>
                            <li>InDesign</li>
                            <li>Flash</li>
                            <li>PHPStorm</li>
                            <li>Eclipse</li>
                            <li>Dreamweaver</li>
                            <li>Zend Studio</li>
                            <li>MS Office</li>
                            <li>MS Visual Studio</li>
                            <li>Autodesk 3D studio Max</li>
                            <li>Netbeans</li>
                        </ul>
                    </div>

                    <div class="grid_4 omega">
                        <h4>Languages</h4>
                        <ul>
                            <li>PHP</li>
                            <li>HTML5/CSS3</li>
                            <li>Advanced Javascript</li>
                            <li>Advanced jQuery</li>
                            <li>Twig</li>
                            <li>ExtJs</li>
                            <li>Basic C/C++/C#</li>
                            <li>Intermediate Java</li>
                            <li><strong>Frameworks</strong></li>
                            <li>Zend Framework</li>
                            <li>Symfony 1.4</li>
                            <li>Advanced Symfony 2.x</li>
                            <li>Intermediate Wordpress</li>
                            <li>Intermediate Drupal</li>
                        </ul>
                    </div>
                    <div class="clear">&nbsp;</div>
                </div>
            </div>

        </div>
    </div>
    <div class="clear">&nbsp;</div>
</div>
<div class="formWrap">
    <div class="formOverlay"></div>
    <div class="formBox">
        <div class="fields">
            <div class="formHeader"><h3>Contact Form <a class="closeForm" id="closeForm" href="#">Close [x]</a></h3></div>							<div class="clear">&nbsp;</div>

            <div id="note"></div>
            <form class="contactForm" action="contact.php" method="post">
                <fieldset>
                    <p><label>Name</label><input class="textbox" type="text" name="name" required="required"/><br class="left" /></p>
                    <p><label>E-Mail</label><input class="textbox" type="email" name="email" required="required" /><br class="left" /></p>
                    <p><label>Subject</label><input class="textbox" type="text" name="subject" required="required" /><br class="left" /></p>
                    <p class="formText"><label>Message</label><textarea class="textbox" name="message" rows="5" cols="25" required="required"></textarea><br class="left" /></p>

                    <p class="formButton"><input class="button" type="submit" name="submit" value="Send Message" /></p>
                </fieldset>
            </form>
        </div>

    </div>
</div>
</body>
</html>